<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\Route;


use Maxipost\CoreDomain\Route\Route;
use Maxipost\CoreStrategyFactories\RouteStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\QueryRequestExecutor;

class RouteGetService
{

    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        QueryRequestExecutor $requestExecutor,
        RouteStrategyFactory $strategyFactory
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(string $id): Route
    {
        $data = $this->requestExecutor->execute(
            str_replace(':id', $id, $this->config->getUriForService(self::class)),
            QueryRequestExecutor::METHOD_GET
        );
        return ($this->strategyFactory)(Route::class)->hydrate($data);
    }
}