FROM php:7.1-cli-alpine3.9

# |--------------------------------------------------------------------------
# | Xdebug
# |--------------------------------------------------------------------------
# |
# | Installs Xdebug, a tool for easily debugging your PHP code.
# |

ENV XDEBUG_VERSION 2.6.0

RUN apk add git &&\
    mkdir -p /usr/src/php/ext/xdebug &&\
    curl -L https://github.com/xdebug/xdebug/archive/$XDEBUG_VERSION.tar.gz | tar xvz -C /usr/src/php/ext/xdebug --strip 1 &&\
    echo 'xdebug' >> /usr/src/php-available-exts &&\
    docker-php-ext-install xdebug &&\
    { \
        echo 'xdebug.remote_enable=on'; \
        echo 'xdebug.remote_autostart=off'; \
        echo 'xdebug.remote_port=9000'; \
        echo 'xdebug.remote_handler=dbgp'; \
        echo 'xdebug.remote_connect_back=0'; \
    } >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

# |--------------------------------------------------------------------------
# | Composer
# |--------------------------------------------------------------------------
# |
# | Installs Composer to easily manage your PHP dependencies.
# |

ENV COMPOSER_VERSION 1.6.2
ENV COMPOSER_ALLOW_SUPERUSER 1

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=$COMPOSER_VERSION &&\
    chmod +x /usr/local/bin/composer

# |--------------------------------------------------------------------------
# | prestissimo
# |--------------------------------------------------------------------------
# |
# | Installs prestissimo to fix Composer performance issues.
# |

# installs prestissimo for the www-data user.
USER www-data

ENV PRESTISSIMO_VERSION 0.3.7

RUN composer global require hirak/prestissimo:$PRESTISSIMO_VERSION

# done, let's go back to root user!
USER root

WORKDIR /app
