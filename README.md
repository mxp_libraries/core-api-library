Install
-------------
```bash
composer require maxipost/core-api-library-lib
```
Configure
-------------
If you need to configure additional paths, they will be merged with default paths, not overridden.
```php
$config = new MXP\CoreApi\Config(
    [
        'paths' => [
            'ClassName::class' => '/url'
        ],
        'url' => 'http://url.to.somewhere',
        'version' => 'v1'
    ]
);
```

#####Default paths
        WarehouseCreateService::class => '/warehouse',
        WarehouseGetService::class => '/warehouse',
        WarehousesGetService::class => '/warehouse/:id',
        WarehouseUpdateService::class => '/warehouse/:id',
        DeliveryZoneCreateService::class => '/deliveryZone',
        DeliveryZoneGetService::class => '/deliveryZone',
        DeliveryZonesGetService::class => '/deliveryZone/:id',
        DeliveryZoneUpdateService::class => '/deliveryZone/:id',
        ContractCreateService::class => '/contract',
        ContractGetService::class => '/contract',
        ContractsGetService::class => '/contract/:id',
        ContractUpdateService::class => '/contract/:id',
        RouteCreateService::class => '/route',
        RouteGetService::class => '/route',
        RoutesGetService::class => '/route/:id',
        RouteUpdateService::class => '/route/:id',
        CounterpartyCreateService::class => '/counterparty',
        CounterpartyGetService::class => '/counterparty',
        CounterpartiesGetService::class => '/counterparties/:id',
        CounterpartyUpdateService::class => '/counterparty/:id',
        OrderServicePointCreateService::class => '/orderServicePoint',
        OrderServicePointGetService::class => '/orderServicePoint',
        OrderServicePointsGetService::class => '/orderServicePoint/:id',
        OrderServicePointUpdateService::class => '/orderServicePoint/:id',
        CourierCreateService::class => '/courier',
        CourierGetService::class => '/courier',
        CouriersGetService::class => '/courier/:id',
        CourierUpdateService::class => '/courier/:id',
        LegalPersonCreateService::class => '/legalPerson',
        LegalPersonGetService::class => '/legalPerson',
        LegalPersonsGetService::class => '/legalPerson/:id',
        LegalPersonUpdateService::class => '/legalPerson/:id',
        OrderCreateService::class => '/order',
        OrderGetService::class => '/order',
        OrdersGetService::class => '/order/:id',
        OrderUpdateService::class => '/order/:id',
        
       
Usage
------------
######Contracts example
You need to use $config from "Configure" section 
#####Create
```php
$contractCreateService = new MXP\CoreApi\Application\Contract\ContractCreateService(
    $config,
    new MXP\CoreApi\Request\CommandRequestExecutor(),
    new Maxipost\CoreStrategyFactories\ContractStrategyFactory(
        new Maxipost\FormStrategy\FormStrategyBuilder()
    )
);
$contractDto = Maxipost\CoreDomain\Contract\Contract();
$contractId = $contractCreateService->execute($contractDto);
```
#####Update
```php
$contractUpdateService = new MXP\CoreApi\Application\Contract\ContractUpdateService(
    $config,
    new MXP\CoreApi\Request\CommandRequestExecutor(),
    new Maxipost\CoreStrategyFactories\ContractStrategyFactory(
            new Maxipost\FormStrategy\FormStrategyBuilder()
    )
);
$contractDto = Maxipost\CoreDomain\Contract\Contract();
$contractId = $contractUpdateService->execute($contractDto);
```
#####Get one
```php
$contractGetService = new MXP\CoreApi\Application\Contract\ContractGetService(
    $config,
    new MXP\CoreApi\Request\QueryRequestExecutor(),
    new Maxipost\CoreStrategyFactories\ContractStrategyFactory(
            new Maxipost\FormStrategy\FormStrategyBuilder()
    )
);
$contractId = 1489;
$contract = $contractGetService->execute($contractId);
```
#####Get many
```php
$contractGetService = new MXP\CoreApi\Application\Contract\ContractsGetService(
    $config,
    new MXP\CoreApi\Request\QueryRequestExecutor(),
    new Maxipost\CoreStrategyFactories\ContractStrategyFactory(
            new Maxipost\FormStrategy\FormStrategyBuilder()
    )
);
$params = [];
$contracts = $contractGetService->execute($params);
```