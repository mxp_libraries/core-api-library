<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\OrderGroup;


use Maxipost\CoreDomain\OrderGroup\OrderGroup;
use Maxipost\CoreStrategyFactories\OrderGroupStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\CommandRequestExecutor;

class OrderGroupCreateService
{

    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        CommandRequestExecutor $requestExecutor,
        OrderGroupStrategyFactory $strategyFactory
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(OrderGroup $dto): string
    {
        $this->requestExecutor->execute(
            $this->config->getUriForService(self::class),
            ($this->strategyFactory)(get_class($dto))->extract($dto),
            CommandRequestExecutor::METHOD_POST
        );
        return $this->requestExecutor->getId();
    }
}