<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\Courier;


use Maxipost\CoreDomain\Courier\Courier;
use Maxipost\CoreStrategyFactories\CourierStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\QueryRequestExecutor;

class CouriersGetService
{

    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        QueryRequestExecutor $requestExecutor,
        CourierStrategyFactory $strategyFactory
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(array $params = []): array
    {
        $data = $this->requestExecutor->execute(
            $this->config->getUriForService(self::class),
            QueryRequestExecutor::METHOD_GET,
            $params
        );
        foreach ($data as &$datum) {
            $datum = ($this->strategyFactory)(Courier::class)->hydrate($datum);
        }
        return $data;
    }
}