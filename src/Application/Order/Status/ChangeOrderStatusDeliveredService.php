<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\Order\Status;


use DateTimeInterface;
use Maxipost\CoreDomain\Order\ValueObject\CashOnDelivery\PaymentType;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\CommandRequestExecutor;

class ChangeOrderStatusDeliveredService
{

    private $config;
    private $requestExecutor;

    public function __construct(
        Config $config,
        CommandRequestExecutor $requestExecutor
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
    }

    public function execute(
        string $orderId,
        PaymentType $paymentType,
        string $comment = '',
        DateTimeInterface $sourceCreatedAt = null
    ): string {
        $data = [
            'paymentType' => $paymentType->getId(),
            'comment' => $comment
        ];
        if ($sourceCreatedAt === null) {
            $sourceCreatedAt = new \DateTimeImmutable();
        }
        $data['sourceCreatedAt'] = $sourceCreatedAt->format(DateTimeInterface::RFC3339_EXTENDED);
        $this->requestExecutor->execute(
            str_replace(':id', $orderId, $this->config->getUriForService(self::class)),
            $data,
            CommandRequestExecutor::METHOD_POST
        );
        return $this->requestExecutor->getId();
    }
}