<?php
declare(strict_types=1);

namespace MXP\CoreApi\Request;


use MXP\CoreApi\Exception\InvalidDataException;
use MXP\CoreApi\Exception\InvalidRequestException;

class CommandRequestExecutor extends CommonRequestExecutor
{


    /**
     * @param string $uri
     * @param mixed $data
     * @param string $method
     * @return void
     * @throws InvalidDataException
     * @throws InvalidRequestException
     */
    public function execute(string $uri, $data, string $method): void
    {
        $this->executeRequest($uri, $method, $data);
    }
}