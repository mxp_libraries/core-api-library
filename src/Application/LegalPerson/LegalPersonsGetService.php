<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\LegalPerson;


use Maxipost\CoreDomain\LegalPerson\LegalPerson;
use Maxipost\CoreStrategyFactories\LegalPersonStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\QueryRequestExecutor;

class LegalPersonsGetService
{

    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        QueryRequestExecutor $requestExecutor,
        LegalPersonStrategyFactory $strategyFactory
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(array $params = []): array
    {
        $data = $this->requestExecutor->execute(
            $this->config->getUriForService(self::class),
            QueryRequestExecutor::METHOD_GET,
            $params
        );
        foreach ($data as &$datum) {
            $datum = ($this->strategyFactory)(LegalPerson::class)->hydrate($datum);
        }
        return $data;
    }
}