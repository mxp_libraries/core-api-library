<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\OrderGroup;


use Maxipost\CoreDomain\OrderGroup\OrderGroup;
use Maxipost\CoreStrategyFactories\OrderGroupStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\QueryRequestExecutor;

class OrderGroupsGetService
{
    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        QueryRequestExecutor $requestExecutor,
        OrderGroupStrategyFactory $strategyFactory
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(array $params = []): array
    {
        $data = $this->requestExecutor->execute(
            $this->config->getUriForService(self::class),
            QueryRequestExecutor::METHOD_GET,
            $params
        );
        foreach ($data as &$datum) {
            $datum = ($this->strategyFactory)(OrderGroup::class)->hydrate($datum);
        }
        return $data;
    }
}