<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\Contract;


use Maxipost\CoreDomain\Contract\Contract;
use Maxipost\CoreStrategyFactories\ContractStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\QueryRequestExecutor;

class ContractGetService
{

    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        QueryRequestExecutor $requestExecutor,
        ContractStrategyFactory $strategyFactory
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(string $id): Contract
    {
        $data = $this->requestExecutor->execute(
            str_replace(':id', $id, $this->config->getUriForService(self::class)),
            QueryRequestExecutor::METHOD_GET
        );
        return ($this->strategyFactory)(Contract::class)->hydrate($data);
    }
}