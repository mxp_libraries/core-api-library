<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\LegalPerson;


use Maxipost\CoreDomain\LegalPerson\LegalPerson;
use Maxipost\CoreStrategyFactories\LegalPersonStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\CommandRequestExecutor;
use function get_class;

class LegalPersonUpdateService
{

    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        CommandRequestExecutor $requestExecutor,
        LegalPersonStrategyFactory $strategyFactory
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(LegalPerson $dto): string
    {
        $this->requestExecutor->execute(
            $this->config->getUriForService(self::class),
            ($this->strategyFactory)(get_class($dto))->extract($dto),
            CommandRequestExecutor::METHOD_POST
        );
        return $this->requestExecutor->getId();
    }
}