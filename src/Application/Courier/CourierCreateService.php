<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\Courier;


use Maxipost\CoreDomain\Courier\Courier;
use Maxipost\CoreStrategyFactories\CourierStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\CommandRequestExecutor;
use function get_class;

class CourierCreateService
{

    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        CommandRequestExecutor $requestExecutor,
        CourierStrategyFactory $strategyFactory
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(Courier $dto): string
    {
        $this->requestExecutor->execute(
            $this->config->getUriForService(self::class),
            ($this->strategyFactory)(get_class($dto))->extract($dto),
            CommandRequestExecutor::METHOD_POST
        );
        return $this->requestExecutor->getId();
    }
}