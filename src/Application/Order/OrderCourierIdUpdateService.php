<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\Order;


use MXP\CoreApi\Config;
use MXP\CoreApi\Request\CommandRequestExecutor;

class OrderCourierIdUpdateService
{

    private $config;
    private $requestExecutor;

    public function __construct(
        Config $config,
        CommandRequestExecutor $requestExecutor
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
    }

    public function execute(string $id, string $courierId): string
    {
        $this->requestExecutor->execute(
            str_replace(':id', $id, $this->config->getUriForService(self::class)),
            $courierId,
            CommandRequestExecutor::METHOD_PATCH
        );
        return $this->requestExecutor->getId();
    }
}