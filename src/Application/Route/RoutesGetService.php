<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\Route;


use Maxipost\CoreDomain\Route\Route;
use Maxipost\CoreStrategyFactories\RouteStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\QueryRequestExecutor;

class RoutesGetService
{

    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        QueryRequestExecutor $requestExecutor,
        RouteStrategyFactory $strategyFactory
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(array $params = []): array
    {
        $data = $this->requestExecutor->execute(
            $this->config->getUriForService(self::class),
            QueryRequestExecutor::METHOD_GET,
            $params
        );
        foreach ($data as &$datum) {
            $datum = ($this->strategyFactory)(Route::class)->hydrate($datum);
        }
        return $data;
    }
}