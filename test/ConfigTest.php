<?php
declare(strict_types=1);

namespace MXP\CoreApiTest;

use MXP\CoreApi\Config;
use PHPUnit\Framework\TestCase;

class ConfigTest extends TestCase
{

    private const TEST_CLASS = 'ns\testClass';
    private const TEST_PATH = '/test';

    public function testGetUriForServiceMustSuccessful(): void
    {
        $config = new Config([
            'paths' => [self::TEST_CLASS => self::TEST_PATH,],
            'version' => Config::CURRENT_VERSION,
            'url' => Config::DEFAULT_URL,
        ]);
        self::assertEquals(
            Config::DEFAULT_URL . Config::VERSION_PATHS[Config::CURRENT_VERSION] . self::TEST_PATH,
            $config->getUriForService(self::TEST_CLASS)
        );
    }

    /**
     * @expectedException \MXP\CoreApi\Exception\InvalidConfigException
     * @expectedExceptionMessage Uri not for ns\testClass not configured
     */
    public function testGetUriForServiceMustThrowExceptionWhenUriNotIsset(): void
    {
        $config = new Config();
        $config->getUriForService(self::TEST_CLASS);
    }
}
