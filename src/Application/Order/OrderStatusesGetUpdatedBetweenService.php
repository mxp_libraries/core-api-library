<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\Order;


use Maxipost\CoreDomain\Order\Order;
use Maxipost\CoreStrategyFactories\OrderStatusStrategyFactory;
use Maxipost\CoreStrategyFactories\OrderStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\CommandRequestExecutor;
use MXP\CoreApi\Request\QueryRequestExecutor;

class OrderStatusesGetUpdatedBetweenService
{

    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        QueryRequestExecutor $requestExecutor,
        OrderStatusStrategyFactory $strategyFactory
    )
    {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(\DateTimeImmutable $dateFrom, \DateTimeImmutable $dateTo = null): Order
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $data = $this->requestExecutor->execute(
            $this->config->getUriForService(self::class),
            CommandRequestExecutor::METHOD_GET, [
                'dateFrom' => $dateFrom->format(DATE_RFC3339_EXTENDED),
                'dateTo' => $dateTo !== null ? $dateTo->format(DATE_RFC3339_EXTENDED) : null
            ]
        );
        return ($this->strategyFactory)(Order::class)->hydrate($data);
    }
}