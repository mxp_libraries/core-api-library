<?php
declare(strict_types=1);

namespace MXP\CoreApiTest\Request;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\SeekException;
use GuzzleHttp\RequestOptions;
use MXP\CoreApi\Exception\InvalidDataException;
use MXP\CoreApi\Exception\InvalidRequestException;
use MXP\CoreApi\Request\CommandRequestExecutor;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class PostRequestTest extends TestCase
{

    private const TEST_URL = 'testUrl';
    private const TEST_DATA = ['testData'];
    private const TEST_TOKEN = 'testToken';
    private const TEST_LOCATION = 'testLocation';
    private const TEST_BODY = '[{"testField":"Invalid"}]';
    private const TEST_INVALID_REQUEST_MSG = 'Invalid request';

    public function testExecuteWhen201StatusCode(): void
    {
        $response = $this->prophesize(ResponseInterface::class);
        $response->getStatusCode()
            ->shouldBeCalled()
            ->willReturn(CommandRequestExecutor::HTTP_CREATED_STATUS_CODE);
        $response->getHeaderLine('Location')
            ->shouldBeCalled()
            ->willReturn(self::TEST_LOCATION);

        $client = $this->prophesize(Client::class);
        $client->request('post', self::TEST_URL, [
            RequestOptions::JSON => self::TEST_DATA,
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . self::TEST_TOKEN,
            ]
        ])
            ->shouldBeCalled()
            ->willReturn($response->reveal());

        $postRequestWithAuth = new CommandRequestExecutor(self::TEST_TOKEN, $client->reveal());
        self::assertEquals(self::TEST_LOCATION, $postRequestWithAuth->execute(self::TEST_URL, self::TEST_DATA));
    }

    public function testExecuteMustThrowExceptionWhen400StatusCode(): void
    {
        $response = $this->prophesize(ResponseInterface::class);
        $response->getStatusCode()
            ->shouldBeCalled()
            ->willReturn(CommandRequestExecutor::HTTP_BAD_REQUEST_STATUS_CODE);
        $stream = $this->prophesize(StreamInterface::class);
        $stream->__toString()
            ->shouldBeCalled()
            ->willReturn(self::TEST_BODY);
        $response->getBody()
            ->shouldBeCalled()
            ->willReturn($stream->reveal());

        $client = $this->prophesize(Client::class);
        $client->request('post', self::TEST_URL, [
            RequestOptions::JSON => self::TEST_DATA,
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . self::TEST_TOKEN,
            ]
        ])
            ->shouldBeCalled()
            ->willReturn($response->reveal());

        $this->expectExceptionObject(new InvalidDataException([['testField' => 'Invalid']]));
        $postRequestWithAuth = new CommandRequestExecutor(self::TEST_TOKEN, $client->reveal());
        $postRequestWithAuth->execute(self::TEST_URL, self::TEST_DATA);
    }

    public function testExecuteMustThrowExceptionWhen500StatusCode(): void
    {
        $response = $this->prophesize(ResponseInterface::class);
        $response->getStatusCode()
            ->shouldBeCalled()
            ->willReturn(500);
        $stream = $this->prophesize(StreamInterface::class);
        $stream->__toString()
            ->shouldBeCalled()
            ->willReturn(self::TEST_INVALID_REQUEST_MSG);
        $response->getBody()
            ->shouldBeCalled()
            ->willReturn($stream->reveal());

        $client = $this->prophesize(Client::class);
        $client->request('post', self::TEST_URL, [
            RequestOptions::JSON => self::TEST_DATA,
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . self::TEST_TOKEN,
            ]
        ])
            ->shouldBeCalled()
            ->willReturn($response->reveal());

        $this->expectExceptionObject(new InvalidRequestException(self::TEST_INVALID_REQUEST_MSG, 500));
        $postRequestWithAuth = new CommandRequestExecutor(self::TEST_TOKEN, $client->reveal());
        $postRequestWithAuth->execute(self::TEST_URL, self::TEST_DATA);
    }

    public function testExecuteMustThrowExceptionWhenGuzzleError(): void
    {
        $stream = $this->prophesize(StreamInterface::class);
        $exception = new SeekException($stream->reveal(), 0, 'test');

        $client = $this->prophesize(Client::class);
        $client->request('post', self::TEST_URL, [
            RequestOptions::JSON => self::TEST_DATA,
            RequestOptions::HEADERS => [
                'Authorization' => 'Bearer ' . self::TEST_TOKEN,
            ]
        ])
            ->shouldBeCalled()
            ->willThrow($exception);

        $this->expectExceptionObject(new InvalidRequestException('test', 0));
        $postRequestWithAuth = new CommandRequestExecutor(self::TEST_TOKEN, $client->reveal());
        $postRequestWithAuth->execute(self::TEST_URL, self::TEST_DATA);
    }
}
