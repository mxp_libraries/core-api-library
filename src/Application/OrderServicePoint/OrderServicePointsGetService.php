<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\OrderServicePoint;


use Maxipost\CoreDomain\OrderServicePoint\OrderServicePoint;
use Maxipost\CoreStrategyFactories\OrderServicePointStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\QueryRequestExecutor;

class OrderServicePointsGetService
{

    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        QueryRequestExecutor $requestExecutor,
        OrderServicePointStrategyFactory $strategyFactory
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(array $params = []): array
    {
        $data = $this->requestExecutor->execute(
            $this->config->getUriForService(self::class),
            QueryRequestExecutor::METHOD_GET,
            $params
        );
        foreach ($data as &$datum) {
            $datum = ($this->strategyFactory)(OrderServicePoint::class)->hydrate($datum);
        }
        return $data;
    }
}