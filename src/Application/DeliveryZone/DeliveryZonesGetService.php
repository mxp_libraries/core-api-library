<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\DeliveryZone;


use Maxipost\CoreDomain\DeliveryZone\DeliveryZone;
use Maxipost\CoreStrategyFactories\DeliveryZoneStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\QueryRequestExecutor;

class DeliveryZonesGetService
{

    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        QueryRequestExecutor $requestExecutor,
        DeliveryZoneStrategyFactory $strategyFactory
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(array $params = []): array
    {
        $data = $this->requestExecutor->execute(
            $this->config->getUriForService(self::class),
            QueryRequestExecutor::METHOD_GET,
            $params
        );
        foreach ($data as &$datum) {
            $datum = ($this->strategyFactory)(DeliveryZone::class)->hydrate($datum);
        }
        return $data;
    }
}