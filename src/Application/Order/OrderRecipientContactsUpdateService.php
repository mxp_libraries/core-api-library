<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\Order;


use Maxipost\CoreDomain\Order\ValueObject\Recipient\Contacts;
use Maxipost\CoreStrategyFactories\OrderRecipientContactsStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\CommandRequestExecutor;
use function get_class;

class OrderRecipientContactsUpdateService
{

    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        CommandRequestExecutor $requestExecutor,
        OrderRecipientContactsStrategyFactory $strategyFactory
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(string $id, Contacts $dto): string
    {
        $this->requestExecutor->execute(
            str_replace(':id', $id, $this->config->getUriForService(self::class)),
            ($this->strategyFactory)(get_class($dto))->extract($dto),
            CommandRequestExecutor::METHOD_PATCH
        );
        return $this->requestExecutor->getId();
    }
}