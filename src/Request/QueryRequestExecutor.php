<?php
declare(strict_types=1);

namespace MXP\CoreApi\Request;


use MXP\CoreApi\Exception\InvalidDataException;
use MXP\CoreApi\Exception\InvalidRequestException;

class QueryRequestExecutor extends CommonRequestExecutor
{


    public const DEFAULT_LIMIT = 100;
    public const DEFAULT_PAGE = 1;

    /**
     * @param string $uri
     * @param string $method
     * @param array $params
     * @return array
     * @throws InvalidDataException
     * @throws InvalidRequestException
     */
    public function execute(string $uri, string $method, array $params = []): array
    {
        $params = array_filter($params);
        $this->executeRequest($uri, $method, $params);
        return $this->content;
    }
}