<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\Order;


use DateTimeImmutable;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\QueryRequestExecutor;

class OrdersStatusUpdatedBetweenGetService
{

    private $config;
    private $requestExecutor;

    public function __construct(
        Config $config,
        QueryRequestExecutor $requestExecutor
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
    }

    /**
     * @param DateTimeImmutable $from
     * @param DateTimeImmutable|null $to
     * @param int $page
     * @param int $limit
     * @return string[] orders ids will returned
     * @throws \MXP\CoreApi\Exception\InvalidConfigException
     * @throws \MXP\CoreApi\Exception\InvalidDataException
     * @throws \MXP\CoreApi\Exception\InvalidRequestException
     */
    public function execute(
        DateTimeImmutable $from,
        DateTimeImmutable $to = null,
        int $page = QueryRequestExecutor::DEFAULT_PAGE,
        int $limit = QueryRequestExecutor::DEFAULT_LIMIT
    ): array {
        $data = $this->requestExecutor->execute(
            $this->config->getUriForService(self::class),
            QueryRequestExecutor::METHOD_GET,
            [
                'dateFrom' => $from->format(DATE_RFC3339_EXTENDED),
                'dateTo' => $to !== null ? $to->format(DATE_RFC3339_EXTENDED) : null,
                'page' => $page,
                'limit' => $limit,
            ]
        );
        return $data;
    }
}