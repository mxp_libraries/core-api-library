<?php
declare(strict_types=1);

namespace MXP\CoreApi\Request;


use Closure;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Message\RequestInterface;
use GuzzleHttp\Message\ResponseInterface;
use MXP\CoreApi\Exception\InvalidDataException;
use MXP\CoreApi\Exception\InvalidRequestException;
use function json_decode;

class CommonRequestExecutor
{
    public const HTTP_SUCCESSFUL = 200;
    public const HTTP_CREATED_STATUS_CODE = 201;
    public const HTTP_BAD_REQUEST_STATUS_CODE = 400;

    public const METHOD_POST = 'post';
    public const METHOD_PATCH = 'patch';
    public const METHOD_PUT = 'put';
    public const METHOD_GET = 'get';

    /**
     * @var Client
     */
    private $client;
    /**
     * @var string
     */
    private $authToken;
    /**
     * @var string
     */
    private $location;
    /**
     * @var array
     */
    protected $content;
    /**
     * @var ResponseInterface
     */
    private $response;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var Closure|array
     */
    private $callback;

    /**
     * @param string $uri
     * @param string $method
     * @param mixed $data
     *
     * @return void
     * @throws InvalidDataException
     * @throws InvalidRequestException
     */
    protected function executeRequest(string $uri, string $method, $data = null): void
    {
        try {
            $this->location = $uri;
            try {
                $this->request = $this->client->createRequest($method, $uri, $this->getOptions($method, $data));
                $this->response = $this->client->send($this->request);
            } catch (RequestException $e) {
                $this->response = $e->getResponse();
                throw $e;
            }

            switch ($this->response->getStatusCode()) {
                case self::HTTP_CREATED_STATUS_CODE:
                    $this->location = $this->response->getHeader('Location');
                    return;
                case self::HTTP_SUCCESSFUL:
                    $this->content = json_decode((string)$this->response->getBody(), true);
                    return;
                case self::HTTP_BAD_REQUEST_STATUS_CODE:
                    throw new InvalidDataException(json_decode((string)$this->response->getBody(), true));
                default:
                    throw new InvalidRequestException((string)$this->response->getBody(),
                        $this->response->getStatusCode());
            }
        } finally {
            $this->callCallback();
        }
    }

    public function __construct(string $authToken, Client $client)
    {
        $this->client = $client;
        $this->authToken = $authToken;
    }

    public function getId(): string
    {
        $parts = explode('/', $this->location);
        return $parts[array_key_last($parts)];
    }

    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @return ResponseInterface
     */
    public function getResponse(): ResponseInterface
    {
        return $this->response;
    }

    /**
     * @return RequestInterface
     */
    public function getRequest(): RequestInterface
    {
        return $this->request;
    }

    /**
     * Выполняется после выполнения запроса.
     *
     * @param Closure|array $callback
     */
    public function setCallback($callback): void
    {
        $this->callback = $callback;
    }

    /**
     * Выполняет функцию обратного вызова, где в аргумент передает текущий instance класса.
     */
    final private function callCallback(): void
    {
        if (!empty($this->callback)) {
            call_user_func($this->callback, $this);
        }
    }

    /**
     * @param mixed $data
     * @param string $method
     *
     * @return array
     */
    private function getOptions(string $method, $data = null): array
    {
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->authToken,
            ],
        ];
        if (in_array($method, [self::METHOD_POST, self::METHOD_PUT, self::METHOD_PATCH], true)) {
            $options['json'] = $data;
        }
        if ($method === self::METHOD_GET) {
            $options['query'] = $data;
        }
        return $options;
    }
}