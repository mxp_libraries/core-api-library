<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\LegalPerson;


use Maxipost\CoreDomain\LegalPerson\LegalPerson;
use Maxipost\CoreStrategyFactories\LegalPersonStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\QueryRequestExecutor;

class LegalPersonGetService
{

    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        QueryRequestExecutor $requestExecutor,
        LegalPersonStrategyFactory $strategyFactory
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(string $id): LegalPerson
    {
        $data = $this->requestExecutor->execute(
            str_replace(':id', $id, $this->config->getUriForService(self::class)),
            QueryRequestExecutor::METHOD_GET
        );
        return ($this->strategyFactory)(LegalPerson::class)->hydrate($data);
    }
}