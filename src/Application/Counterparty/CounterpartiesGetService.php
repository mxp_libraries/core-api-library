<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\Counterparty;


use Maxipost\CoreDomain\Counterparty\Counterparty;
use Maxipost\CoreStrategyFactories\CounterpartyStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\QueryRequestExecutor;

class CounterpartiesGetService
{

    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        QueryRequestExecutor $requestExecutor,
        CounterpartyStrategyFactory $strategyFactory
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(array $params = []): array
    {
        $data = $this->requestExecutor->execute(
            $this->config->getUriForService(self::class),
            QueryRequestExecutor::METHOD_GET,
            $params
        );
        foreach ($data as &$datum) {
            $datum = ($this->strategyFactory)(Counterparty::class)->hydrate($datum);
        }
        return $data;
    }
}