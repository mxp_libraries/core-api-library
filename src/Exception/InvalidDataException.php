<?php
declare(strict_types=1);

namespace MXP\CoreApi\Exception;


use Throwable;

/**
 * @codeCoverageIgnore
 */
class InvalidDataException extends \Exception
{

    private $payload;

    public function __construct(array $payload, string $message = '', int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->payload = $payload;
    }

    /**
     * @return array
     */
    public function getPayload(): array
    {
        return $this->payload;
    }


}