<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\Order\Status;


use DateTimeImmutable;
use DateTimeInterface;
use Maxipost\CoreDomain\Order\ValueObject\CashOnDelivery\PaymentType;
use Maxipost\CoreDomain\Order\ValueObject\ExtendedStatuses\Goods;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\CommandRequestExecutor;

class ChangeOrderStatusPartiallyDeliveredService
{

    private $config;
    private $requestExecutor;

    public function __construct(
        Config $config,
        CommandRequestExecutor $requestExecutor
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
    }

    /**
     * @param string $orderId
     * @param PaymentType $paymentType
     * @param Goods[] $goods
     * @param string $comment
     * @param DateTimeInterface|null $sourceCreatedAt
     * @return string
     * @throws \MXP\CoreApi\Exception\InvalidConfigException
     * @throws \MXP\CoreApi\Exception\InvalidDataException
     * @throws \MXP\CoreApi\Exception\InvalidRequestException
     */
    public function execute(
        string $orderId,
        PaymentType $paymentType,
        array $goods,
        string $comment = '',
        DateTimeInterface $sourceCreatedAt = null
    ): string {

        foreach ($goods as &$good) {
            $good = [
                'itemId' => $good->getItemId(),
                'count' => $good->getCount(),
            ];
        }
        unset($good);

        $data = [
            'paymentType' => $paymentType->getId(),
            'comment' => $comment,
            'goods' => $goods,
        ];
        if ($sourceCreatedAt === null) {
            $sourceCreatedAt = new DateTimeImmutable();
        }
        $data['sourceCreatedAt'] = $sourceCreatedAt->format(DateTimeInterface::RFC3339_EXTENDED);
        $this->requestExecutor->execute(
            str_replace(':id', $orderId, $this->config->getUriForService(self::class)),
            $data,
            CommandRequestExecutor::METHOD_POST
        );
        return $this->requestExecutor->getId();
    }
}