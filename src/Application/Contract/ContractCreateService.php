<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\Contract;


use Maxipost\CoreDomain\Contract\Contract;
use Maxipost\CoreStrategyFactories\ContractStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\CommandRequestExecutor;
use function get_class;

class ContractCreateService
{

    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        CommandRequestExecutor $requestExecutor,
        ContractStrategyFactory $strategyFactory
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(Contract $dto): string
    {
        $this->requestExecutor->execute(
            $this->config->getUriForService(self::class),
            ($this->strategyFactory)(get_class($dto))->extract($dto),
            CommandRequestExecutor::METHOD_POST
        );
        return $this->requestExecutor->getId();
    }
}