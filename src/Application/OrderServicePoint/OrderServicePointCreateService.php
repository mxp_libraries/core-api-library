<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\OrderServicePoint;


use Maxipost\CoreDomain\OrderServicePoint\OrderServicePoint;
use Maxipost\CoreStrategyFactories\OrderServicePointStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\CommandRequestExecutor;
use function get_class;

class OrderServicePointCreateService
{

    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        CommandRequestExecutor $requestExecutor,
        OrderServicePointStrategyFactory $strategyFactory
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(OrderServicePoint $dto): string
    {
        $this->requestExecutor->execute(
            $this->config->getUriForService(self::class),
            ($this->strategyFactory)(get_class($dto))->extract($dto),
            CommandRequestExecutor::METHOD_POST
        );
        return $this->requestExecutor->getId();
    }
}