<?php
declare(strict_types=1);

namespace MXP\CoreApi\Application\DeliveryZone;


use Maxipost\CoreDomain\DeliveryZone\DeliveryZone;
use Maxipost\CoreStrategyFactories\DeliveryZoneStrategyFactory;
use MXP\CoreApi\Config;
use MXP\CoreApi\Request\QueryRequestExecutor;

class DeliveryZoneGetService
{

    private $config;
    private $requestExecutor;
    private $strategyFactory;

    public function __construct(
        Config $config,
        QueryRequestExecutor $requestExecutor,
        DeliveryZoneStrategyFactory $strategyFactory
    ) {
        $this->config = $config;
        $this->requestExecutor = $requestExecutor;
        $this->strategyFactory = $strategyFactory;
    }

    public function execute(string $id): DeliveryZone
    {
        $data = $this->requestExecutor->execute(
            str_replace(':id', $id, $this->config->getUriForService(self::class)),
            QueryRequestExecutor::METHOD_GET
        );
        return ($this->strategyFactory)(DeliveryZone::class)->hydrate($data);
    }
}