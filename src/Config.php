<?php
declare(strict_types=1);

namespace MXP\CoreApi;


use Maxipost\CoreDomain\Contract\Contract;
use Maxipost\CoreDomain\Counterparty\Counterparty;
use Maxipost\CoreDomain\Courier\Courier;
use Maxipost\CoreDomain\DeliveryZone\DeliveryZone;
use Maxipost\CoreDomain\LegalPerson\LegalPerson;
use Maxipost\CoreDomain\Order\Order;
use Maxipost\CoreDomain\OrderServicePoint\OrderServicePoint;
use Maxipost\CoreDomain\Route\Route;
use Maxipost\CoreDomain\Warehouse\Warehouse;
use Maxipost\CoreStrategyFactories\ContractStrategyFactory;
use Maxipost\CoreStrategyFactories\CounterpartyStrategyFactory;
use Maxipost\CoreStrategyFactories\CourierStrategyFactory;
use Maxipost\CoreStrategyFactories\DeliveryZoneStrategyFactory;
use Maxipost\CoreStrategyFactories\LegalPersonStrategyFactory;
use Maxipost\CoreStrategyFactories\OrderServicePointStrategyFactory;
use Maxipost\CoreStrategyFactories\OrderStrategyFactory;
use Maxipost\CoreStrategyFactories\RouteStrategyFactory;
use Maxipost\CoreStrategyFactories\WarehouseStrategyFactory;
use MXP\CoreApi\Application\Contract\ContractCreateService;
use MXP\CoreApi\Application\Contract\ContractGetService;
use MXP\CoreApi\Application\Contract\ContractsGetService;
use MXP\CoreApi\Application\Contract\ContractUpdateService;
use MXP\CoreApi\Application\Counterparty\CounterpartiesGetService;
use MXP\CoreApi\Application\Counterparty\CounterpartyCreateService;
use MXP\CoreApi\Application\Counterparty\CounterpartyGetService;
use MXP\CoreApi\Application\Counterparty\CounterpartyUpdateService;
use MXP\CoreApi\Application\Courier\CourierCreateService;
use MXP\CoreApi\Application\Courier\CourierGetService;
use MXP\CoreApi\Application\Courier\CouriersGetService;
use MXP\CoreApi\Application\Courier\CourierUpdateService;
use MXP\CoreApi\Application\DeliveryZone\DeliveryZoneCreateService;
use MXP\CoreApi\Application\DeliveryZone\DeliveryZoneGetService;
use MXP\CoreApi\Application\DeliveryZone\DeliveryZonesGetService;
use MXP\CoreApi\Application\DeliveryZone\DeliveryZoneUpdateService;
use MXP\CoreApi\Application\LegalPerson\LegalPersonCreateService;
use MXP\CoreApi\Application\LegalPerson\LegalPersonGetService;
use MXP\CoreApi\Application\LegalPerson\LegalPersonsGetService;
use MXP\CoreApi\Application\LegalPerson\LegalPersonUpdateService;
use MXP\CoreApi\Application\Order\OrderCashOnDeliveryPaymentTypeUpdateService;
use MXP\CoreApi\Application\Order\OrderCourierIdUpdateService;
use MXP\CoreApi\Application\Order\OrderCreateService;
use MXP\CoreApi\Application\Order\OrderDelivererNotesUpdateService;
use MXP\CoreApi\Application\Order\OrderDeliveryDateUpdateService;
use MXP\CoreApi\Application\Order\OrderDeliveryOrderServicePointIdUpdateService;
use MXP\CoreApi\Application\Order\OrderDeliveryZoneIdUpdateService;
use MXP\CoreApi\Application\Order\OrderDimensionsUpdateService;
use MXP\CoreApi\Application\Order\OrderGetService;
use MXP\CoreApi\Application\Order\OrderGoodsUpdateService;
use MXP\CoreApi\Application\Order\OrderRecipientAddressUpdateService;
use MXP\CoreApi\Application\Order\OrderRecipientContactsUpdateService;
use MXP\CoreApi\Application\Order\OrdersGetService;
use MXP\CoreApi\Application\Order\OrdersStatusUpdatedBetweenGetService;
use MXP\CoreApi\Application\Order\OrdersUpdatedBetweenGetService;
use MXP\CoreApi\Application\Order\OrderUpdateService;
use MXP\CoreApi\Application\Order\Status\ChangeOrderStatusDeliveredService;
use MXP\CoreApi\Application\Order\Status\ChangeOrderStatusDeliveryFailuredService;
use MXP\CoreApi\Application\Order\Status\ChangeOrderStatusFailureWithoutDeliveryService;
use MXP\CoreApi\Application\Order\Status\ChangeOrderStatusPartiallyDeliveredService;
use MXP\CoreApi\Application\Order\Status\OrderChangeStatusService;
use MXP\CoreApi\Application\OrderServicePoint\OrderServicePointCreateService;
use MXP\CoreApi\Application\OrderServicePoint\OrderServicePointGetService;
use MXP\CoreApi\Application\OrderServicePoint\OrderServicePointsGetService;
use MXP\CoreApi\Application\OrderServicePoint\OrderServicePointUpdateService;
use MXP\CoreApi\Application\Route\RouteCreateService;
use MXP\CoreApi\Application\Route\RouteGetService;
use MXP\CoreApi\Application\Route\RoutesGetService;
use MXP\CoreApi\Application\Route\RouteUpdateService;
use MXP\CoreApi\Application\Warehouse\WarehouseCreateService;
use MXP\CoreApi\Application\Warehouse\WarehouseGetService;
use MXP\CoreApi\Application\Warehouse\WarehousesGetService;
use MXP\CoreApi\Application\Warehouse\WarehouseUpdateService;
use MXP\CoreApi\Exception\InvalidConfigException;

class Config
{

    public const VERSION_V1 = 'v1';

    public const VERSION_PATHS = [
        self::VERSION_V1 => '',
    ];

    public const CURRENT_VERSION = self::VERSION_V1;

    public const DEFAULT_PATHS = [
        WarehouseCreateService::class => '/warehouses',
        WarehouseGetService::class => '/warehouses',
        WarehousesGetService::class => '/warehouses/:id',
        WarehouseUpdateService::class => '/warehouses/:id',
        DeliveryZoneCreateService::class => '/deliveryZones',
        DeliveryZoneGetService::class => '/deliveryZones',
        DeliveryZonesGetService::class => '/deliveryZones/:id',
        DeliveryZoneUpdateService::class => '/deliveryZones/:id',
        ContractCreateService::class => '/contracts',
        ContractGetService::class => '/contracts',
        ContractsGetService::class => '/contracts/:id',
        ContractUpdateService::class => '/contracts/:id',
        RouteCreateService::class => '/routes',
        RouteGetService::class => '/routes',
        RoutesGetService::class => '/routes/:id',
        RouteUpdateService::class => '/routes/:id',
        CounterpartyCreateService::class => '/counterparties',
        CounterpartyGetService::class => '/counterparties',
        CounterpartiesGetService::class => '/counterparties/:id',
        CounterpartyUpdateService::class => '/counterparties/:id',
        OrderServicePointCreateService::class => '/orderServicePoints',
        OrderServicePointGetService::class => '/orderServicePoints',
        OrderServicePointsGetService::class => '/orderServicePoints/:id',
        OrderServicePointUpdateService::class => '/orderServicePoints/:id',
        CourierCreateService::class => '/couriers',
        CourierGetService::class => '/couriers',
        CouriersGetService::class => '/couriers/:id',
        CourierUpdateService::class => '/couriers/:id',
        LegalPersonCreateService::class => '/legalPersons',
        LegalPersonGetService::class => '/legalPersons',
        LegalPersonsGetService::class => '/legalPerson/:id',
        LegalPersonUpdateService::class => '/legalPersons/:id',
        OrderCreateService::class => '/orders',
        OrdersGetService::class => '/orders',
        OrderGetService::class => '/orders/:id',
        OrderUpdateService::class => '/orders/:id',
        OrdersUpdatedBetweenGetService::class => '/ordersUpdatedBetween',
        OrdersStatusUpdatedBetweenGetService::class => '/ordersStatusUpdatedBetween',
        OrderCashOnDeliveryPaymentTypeUpdateService::class => '/orders/:id/cashOnDelivery/paymentType',
        OrderCourierIdUpdateService::class => '/orders/:id/serviceInfo/courierId',
        OrderDelivererNotesUpdateService::class => '/orders/:id/deliverer/notes',
        OrderDeliveryDateUpdateService::class => '/orders/:id/orderDelivery/date',
        OrderDeliveryOrderServicePointIdUpdateService::class => '/orders/:id/orderDelivery/orderServicePointId',
        OrderDeliveryZoneIdUpdateService::class => '/orders/:id/serviceInfo/deliveryZoneId',
        OrderDimensionsUpdateService::class => '/orders/:id/dimensions',
        OrderGoodsUpdateService::class => '/orders/:id/goods',
        OrderRecipientAddressUpdateService::class => '/orders/:id/recipient/address',
        OrderRecipientContactsUpdateService::class => '/orders/:id/recipient/contacts',
        ChangeOrderStatusDeliveredService::class => '/orders/:id/extendedStatuses/delivered',
        ChangeOrderStatusDeliveryFailuredService::class => '/orders/:id/extendedStatuses/deliveryFailured',
        ChangeOrderStatusFailureWithoutDeliveryService::class => '/orders/:id/extendedStatuses/failureWithoutDelivery',
        ChangeOrderStatusPartiallyDeliveredService::class => '/orders/:id/extendedStatuses/partiallyDelivered',
        OrderChangeStatusService::class => '/orders/:id/statuses',
    ];

    public const DTO_FOR_FACTORY = [
        WarehouseStrategyFactory::class => Warehouse::class,
        OrderStrategyFactory::class => Order::class,
        OrderServicePointStrategyFactory::class => OrderServicePoint::class,
        CourierStrategyFactory::class => Courier::class,
        ContractStrategyFactory::class => Contract::class,
        CounterpartyStrategyFactory::class => Counterparty::class,
        DeliveryZoneStrategyFactory::class => DeliveryZone::class,
        LegalPersonStrategyFactory::class => LegalPerson::class,
        RouteStrategyFactory::class => Route::class,
    ];

    public const DEFAULT_URL = '';
    private $paths;
    private $url;
    private $version;

    public function __construct(array $config = [])
    {
        $this->paths = isset($config['paths'])
            ? array_merge(self::DEFAULT_PATHS, $config['paths'])
            : self::DEFAULT_PATHS;
        $this->url = $config['url'] ?? self::DEFAULT_URL;
        $this->version = $config['version'] ?? self::CURRENT_VERSION;


    }

    /**
     * @param string $className
     * @return string
     */
    public function getUriForService(string $className): string
    {
        if (!isset($this->paths[$className])) {
            throw new InvalidConfigException("Uri for $className not configured");
        }
        return $this->url . self::VERSION_PATHS[$this->version] . $this->paths[$className];
    }
}